This is a mini project for a job interview.

# TASK

Recognize pictures of X and O
1.	obtain testing data (picture of 10x10 px from internet, own photos and/or own pictures)
2.	create model using NN (Neural Network)
3.	train NN
4.	evaluate NN
5.	create stand alone application which is using camera as input and classification of X and O as output

# RUN

1. (Optional) Run **train.py** to generate fresh model (or skip this point and use trained model in `models/`):  
    `$ python train.py`

2. Run **run.py** to start capturing and predicting images created by the webcam:  
    `$ python run.py`
