# Author: Adam Varga, varga.adam3@gmail.com
# Predict O's and X's from captured images

from tensorflow import keras
from keras.preprocessing import image
import numpy as np
import tensorflow as tf
import cv2

img_width, img_height = 10, 10
img_counter = 0
result = ''

# load own model
my_model = keras.models.load_model(u'models/trained_model.h5')

cv2.namedWindow('Capturing image')
vc = cv2.VideoCapture(0)

if vc.isOpened():
    rval, frame = vc.read()
else:
    rval = False

while rval:
    cv2.imshow('Capturing image', frame)
    rval, frame = vc.read()
    key = cv2.waitKey(1)
    if key == 27: # exit on ESC
        break
    if key == 32: # capture img on SPACE
        img_name = 'opencv_frame_{}.jpg'.format(img_counter)
        img_dir = 'captured_images/'
        cv2.imwrite(img_dir + img_name, frame)
        cv2.imshow(img_dir + img_name, frame)
        print(img_dir + '{} written!'.format(img_name))

        print('Predicting ' + img_dir + img_name)
        img = image.load_img(img_dir + img_name, target_size=(img_width, img_height))
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        result = my_model.predict(img)
        if np.rint(result[0]) == 1:
            print('Result: x (%s)' % (result[0]))
        else:
            print('Result: o (%s)' % (result[0]))

        img_counter += 1

vc.release()
cv2.destroyWindow('Capturing image')