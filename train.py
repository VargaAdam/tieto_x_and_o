# Author: Adam Varga, varga.adam3@gmail.com
# Train classifier model for recognize picture of X and O

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
import numpy as np
import matplotlib.pyplot as plt

train_data_dir = u'C:\\praca\\ponuky\\tieto\\tieto_x_and_o\\data\\train'
validation_data_dir = u'C:\\praca\\ponuky\\tieto\\tieto_x_and_o\\data\\valid'
test_data_dir = u'C:\\praca\\ponuky\\tieto\\tieto_x_and_o\\data\\test'

nb_train_samples = 240
nb_validation_samples = 32
epochs = 300
batch_size = 64
img_width, img_height = 10, 10

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

# architecture of NN
model = Sequential()
model.add(Conv2D(64, (2, 2), input_shape=(img_width, img_height, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (2, 2)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

train_datagen = ImageDataGenerator(
	rescale=1./255,
	shear_range=0.2,
	zoom_range=0.2,
	horizontal_flip=True,
	#fill_mode = "nearest",
	#brightness_range=[0.2,1.0],
	#rotation_range = 60,
	#width_shift_range = 0.1,
	#height_shift_range = 0.1,
	#vertical_flip=True
	)

test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
	train_data_dir,
	target_size=(img_width, img_height),
	batch_size=batch_size,
	class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
	validation_data_dir,
	target_size=(img_width, img_height),
	batch_size=batch_size,
	class_mode='binary')

history = model.fit_generator(
	train_generator,
	steps_per_epoch=nb_train_samples // batch_size,
	epochs=epochs,
	validation_data=validation_generator,
	validation_steps=nb_validation_samples // batch_size)

# list all data in history
print(history.history.keys())

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

test_datagen = ImageDataGenerator(rescale=1./255)

test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_width, img_height),
        color_mode="rgb",
        shuffle=False,
        class_mode='binary',
        batch_size=1)

filenames = test_generator.filenames
nb_samples = len(filenames)

predict = model.predict_generator(test_generator, steps=nb_samples)

# prediction from test dataset
for i in range(nb_samples):
    if np.rint(predict[i]) == 1:
        print('%s is x (%s)' % (filenames[i], predict[i][0]))
    else:
        print('%s is o (%s)' % (filenames[i], predict[i][0]))

results = model.evaluate(test_generator)
print('test loss, test acc:', results)

model.save('trained_model.h5')